# Best fit rigid body transformation using simple ICP algorithm

I implement an SVD-based least-squared best-fit algorithm for corresponding 3D point clouds. Everything is implemented from scratch to have a base level understanding of the Iterative Closest Point(ICP) algorithm. I refereed Clay Flannigan's repo : https://github.com/ClayFlannigan/icp.
The whole thing is implemented as a ROS Node via Catkin workspace.
